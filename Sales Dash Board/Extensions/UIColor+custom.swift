//
//  UIColor+custom.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 01/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

extension UIColor{
    
    static func primary() -> UIColor{
       return UIColor(netHex: 0x8FC28A)
    }
    static func secondary() -> UIColor{
        return UIColor(netHex: 0xE8963E)
    }
    
    static func header() -> UIColor{
        return UIColor(netHex: 0xFFEB3B)
    }
    
    static func unselected() -> UIColor{
        return UIColor.lightGray
    }
    
    
    
    private convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    public convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
    }
    
}
