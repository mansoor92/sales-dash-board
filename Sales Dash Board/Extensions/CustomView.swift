//
//  CustomView.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 01/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

//Load view from xib file
public protocol CustomView{
    init(frame: CGRect)
}
public extension CustomView where Self: UIView, Self: NibLoadableView{
    
    public func commonInit(bundle: Bundle) -> UIView{
        let nib = bundle.loadNibNamed(Self.nibName, owner: self, options: nil)
        let tempView = nib?.first as? UIView
        guard (tempView) != nil else {
            fatalError("unable to load custom view")
        }
        tempView!.frame = self.bounds
        tempView!.autoresizingMask = [.flexibleWidth,.flexibleHeight]
        self.addSubview(tempView!)
        return tempView!
    }
}


