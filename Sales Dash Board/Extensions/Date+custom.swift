//
//  Date+custom.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 01/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation

extension Date{
    public static func stringFromDate(date:Date,format:String="yyyy-MM-dd'T'HH:mm:ss.SSSZ")->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        return dateFormatter.string(from: date)
    }
    
    public func string(format:String="yyyy-MM-dd'T'HH:mm:ss.SSSZ") -> String {
        return Date.stringFromDate(date: self, format: format)
    }
}
