//
//  NibLoadable.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

public protocol StoryBoardLoadableView{}

public extension StoryBoardLoadableView where Self: UIViewController{
    public static var sceneName: String {
        let vc = String(describing: self)
        let str = vc.replacingOccurrences(of: "ViewController", with: "Scene")
        return str
    }
}
