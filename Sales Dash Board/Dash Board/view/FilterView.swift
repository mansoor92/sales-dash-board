//
//  FilterView.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 01/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit
import RxSwift

protocol FilterViewDelegate {
    func showDateLabels()
    func hideDateLabels()
    func showDatePicker(from: Date, to: Date, isSelectedFrom: Bool)
}
class FilterView: UIView, NibLoadableView, CustomView {

    var contentView: UIView!
    
    @IBOutlet var filters: [UIButton]!
    @IBOutlet var filterArrow: UIImageView!
    @IBOutlet weak var dateContainerHeight: NSLayoutConstraint!
    @IBOutlet weak var productTypeView: ProductTypeView!
    @IBOutlet weak var productTypeViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var btnFromDate: UIButton!
    @IBOutlet weak var btnToDate: UIButton!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    var selectedFilterIndex = 0
    var selectedFilter = Variable<FilterType>(.today)
    var filtersTypes: [FilterType] = [.today,.week,.month,.tillnow,.annual,.custom]
    
    var delegate: FilterViewDelegate?
    var toDate = Variable(Date())
    var fromDate = Variable(Date())
	var selectedSegmentIndex: Observable<Int>? {
		didSet {
			//show/hide product type on segment change
			selectedSegmentIndex?.subscribe(onNext: { [weak self] (index) in
				let productTypeHeight: CGFloat = (index == 0) ? 57 : 0
				self?.productTypeViewHeight.constant = productTypeHeight
				UIView.animate(withDuration: 0.3) { [weak self] in
					self?.superview?.superview?.layoutIfNeeded()
				}
			}).disposed(by: disposeBag)
		}
	}
    
    @IBOutlet weak var dateContainer: UIView!
    
    let disposeBag = DisposeBag()
    
    static func instance() -> FilterView {
        let bundle = Bundle(for: FilterView.classForCoder())
        let nib = UINib(nibName: FilterView.nibName, bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! FilterView
        return view
    }
    
    override required init(frame: CGRect) {
        super.init(frame: frame)
//        contentView = commonInit(bundle: .main)
//        configView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
//        contentView = commonInit(bundle: .main)
//        configView()
    }
    
    override func awakeFromNib() {
        configView()
    }
    func configView()  {
        for i in 0..<filters.count{
            filters[i].addTarget(self, action: #selector(actSelected(sender:)), for: .touchUpInside)
        }
        
        toDate.asDriver()
            .skip(1)
            .map{$0.string(format: Filter.dateFormat)}
            .drive(btnToDate.rx.title(for: .normal))
            .disposed(by: disposeBag)
        
        fromDate.asDriver()
            .skip(1)
            .map{$0.string(format: Filter.dateFormat)}
            .drive(btnFromDate.rx.title(for: .normal))
            .disposed(by: disposeBag)
    }

    @objc func actSelected(sender: UIButton)  {
        //filter is not already selected
        guard sender != filters[selectedFilterIndex] else {
            return
        }
        
        //custom filter is selected
        if selectedFilterIndex == filters.count - 1{
            delegate?.hideDateLabels()
        }
        
        //desselect selected filter
        changeBtnState(btn: filters[selectedFilterIndex], state: false)
        
        //select new filter
        for i in 0..<filters.count{
            if sender == filters[i]{
                selectedFilterIndex = i
                changeBtnState(btn: sender, state: true)
                //custom filter is selected
                if selectedFilterIndex == filters.count - 1{
                    delegate?.showDateLabels()
                }else{
                    selectedFilter.value = filtersTypes[selectedFilterIndex]
                }
            }
        }
    }
    
    func changeBtnState(btn: UIButton, state: Bool)  {
        if state{
            btn.backgroundColor = UIColor.primary()
        }else{
            btn.backgroundColor = UIColor.unselected()
        }
    }
}

//MARK:- Date
extension FilterView{
    func showDate()  {
        self.dateContainerHeight.constant = 44
//        containerheight.constant = 59+44
        toDate.value = Date()
        fromDate.value = Date()
        UIView.animate(withDuration: 0.3) {
			self.superview?.superview?.layoutIfNeeded()
        }
    }
    
    func hideDate()  {
        self.dateContainerHeight.constant = 0
//        containerheight.constant = 59
        UIView.animate(withDuration: 0.3) {
			self.superview?.superview?.layoutIfNeeded()
        }
    }

    @IBAction func actShowDatePicker(_ sender: UIButton) {
        var isSelectedFrom = true
        if sender == btnToDate{
            isSelectedFrom = false
        }
        delegate?.showDatePicker(from:fromDate.value, to: toDate.value, isSelectedFrom: isSelectedFrom)
    }
    
    @IBAction func actApplyCustomFilter(_ sender: UIButton) {
        selectedFilter.value = .custom
    }
}
