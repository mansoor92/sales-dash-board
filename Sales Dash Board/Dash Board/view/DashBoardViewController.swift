//
//  ViewController.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import RxCocoa

class DashBoardViewController: UIViewController, StoryBoardLoadableView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var viewModel: DashBoardViewModel!
    let disposeBag = DisposeBag()
    var filterView: FilterView?
    var datePickerView: DatePickerView?
    @IBOutlet weak var filterContainer: UIView!
    @IBOutlet weak var filterContainerHeight: NSLayoutConstraint!
    //    @IBOutlet weak var collectionViewBottom: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        configView()
        collectionView.register(SaleCollectionViewCell.self)
        
		Observable.combineLatest(segmentedControl.rx.selectedSegmentIndex.asObservable(), filterView!.selectedFilter.asObservable(),filterView!.productTypeView.selectedFilter.asObservable()) { (index, filterType, productType) -> Filter in
                let filter = Filter()
                filter.saleType = (index == 0) ? .sale : .collection
                filter.type = filterType
				filter.productType = productType
                filter.toDate = self.filterView?.toDate.value
                filter.fromDate = self.filterView?.fromDate.value
                return filter
            }.asDriver(onErrorJustReturn: Filter())
            .drive(viewModel.filter)
            .disposed(by: disposeBag)
        
        viewModel.apiCallStatus
            .subscribe(onNext: { (apiCallStatus) in
                switch apiCallStatus{
                case .loading:
                    print("Loading")
                    self.filterView?.activityIndicator.startAnimating()
                case .refreshData:
                    self.filterView?.activityIndicator.stopAnimating()
                    self.collectionView.reloadData()
                case .showError(let err):
                    self.filterView?.activityIndicator.stopAnimating()
                    let error = ResponseError(error: err)
                    let actRetry = UIAlertAction(title: "Retry", style: .default, handler: { (act) in
                        self.filterView?.selectedFilter.value = self.filterView!.selectedFilter.value
                    })
                    let actDismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
                    Alert.showMessage(viewController: self, title: "Unable to Fetch Data", msg: error.reason, actions: [actDismiss,actRetry], showOnlyDismissBtn: false)
                }
            }).disposed(by: disposeBag)

		
    }
}

//MARK:- FilterView
extension DashBoardViewController: FilterViewDelegate{
    func configView()  {
        segmentedControl.tintColor = UIColor.secondary()
        filterView = FilterView.instance()
		filterView?.selectedSegmentIndex = segmentedControl.rx.selectedSegmentIndex.asObservable()
        let leading = NSLayoutConstraint(item: filterView!, attribute: .leading, relatedBy: .equal, toItem: filterContainer, attribute: .leading, multiplier: 1, constant: 0)
        let trailing = NSLayoutConstraint(item: filterContainer, attribute: .trailing, relatedBy: .equal, toItem: filterView!, attribute: .trailing, multiplier: 1, constant: 0)
        let top = NSLayoutConstraint(item: filterView!, attribute: .top, relatedBy: .equal, toItem: filterContainer, attribute: .top, multiplier: 1, constant: 0)
        let bottom = NSLayoutConstraint(item: filterContainer, attribute: .bottom, relatedBy: .equal, toItem: filterView!, attribute: .bottom, multiplier: 1, constant: 0)
        filterView?.delegate = self
        filterContainer.addSubview(filterView!)
        filterContainer.addConstraints([top,bottom,leading,trailing])
    }    
    
//    FilterViewDelegate
    func showDateLabels() {
//        filterContainerHeight.constant = 59+44
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//           self.filterView?.frame = CGRect(x: 0, y: self.view.frame.size.height-59-4-44, width: self.view.frame.size.width, height: 59+44)
//        }
        filterView?.showDate()
//        collectionViewBottom.constant = 64+44
    }
    
    func hideDateLabels() {
//        filterContainerHeight.constant = 59
//        UIView.animate(withDuration: 0.3) {
//            self.view.layoutIfNeeded()
//            self.filterView?.frame = CGRect(x: 0, y: self.view.frame.size.height-59-4, width: self.view.frame.size.width, height: 59)
//        }
        filterView?.hideDate()
//        collectionViewBottom.constant = 64
    }
    
    func showDatePicker(from: Date, to: Date, isSelectedFrom: Bool) {
        if datePickerView == nil{
            datePickerView = DatePickerView.instance()
        }
        datePickerView?.delegate = self
        datePickerView?.config(from: from, to: to, isSelectedFrom: isSelectedFrom)
        self.view.addSubview(datePickerView!)
        datePickerView?.show()
    }
}

extension DashBoardViewController: DatePickerViewDelegate{
    
    func applyDateFilter(from: Date, to: Date) {
        datePickerView?.hide {
            self.datePickerView?.removeFromSuperview()
        }
        filterView?.toDate.value = to
        filterView?.fromDate.value = from
    }
}

extension DashBoardViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.noOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let saleCell = collectionView.dequeCell(indexPath: indexPath) as SaleCollectionViewCell
        saleCell.config(saleViewModel: viewModel.dataForItem(atIndexPath: indexPath))
        if indexPath.row == 0{
            saleCell.style.onNext(.header)
        }else if indexPath.row == 1{
            saleCell.style.onNext(.subhead)
        }else if indexPath.row == viewModel.noOfItems() - 1 {
            saleCell.style.onNext(.total)
        }else{
           saleCell.style.onNext(.body)
        }
        return saleCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = self.view.frame.size.width - 24
        let h: CGFloat = 50
        return CGSize(width: w, height: h)
    }
}


