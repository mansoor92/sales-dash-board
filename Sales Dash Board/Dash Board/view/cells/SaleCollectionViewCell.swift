//
//  SaleCollectionViewCell.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa



class SaleCollectionViewCell: UICollectionViewCell, NibLoadableView {

    enum Style {
        case header
        case subhead
        case body
        case total
        
        func styling() -> (sales: UIColor, stats: UIColor, currentYear: UIColor, currentYearTextColor: UIColor) {
            var salesColor = UIColor.secondary()
            let statsColor: UIColor!
            let currentYear: UIColor!
            var currentYearTextColor = UIColor.black
            switch self {
            case .header:
                salesColor = UIColor.header().withAlphaComponent(0.7)
                statsColor = UIColor.header().withAlphaComponent(0.7)
                currentYear = UIColor.header().withAlphaComponent(0.7)
            case .subhead:
                statsColor = UIColor(netHex: 0x00897B).withAlphaComponent(0.7)
                salesColor = UIColor(netHex: 0x00897B).withAlphaComponent(0.7)
                currentYear = UIColor(netHex: 0xFF8A80)
                currentYearTextColor = UIColor.white
            case .body:
                salesColor = UIColor.primary().withAlphaComponent(0.7)
                statsColor = UIColor.primary().withAlphaComponent(0.7)
                currentYear = UIColor(netHex: 0xFF8A80)
                currentYearTextColor = UIColor.white
            case .total:
                salesColor = UIColor(netHex: 0xFFC107)
                statsColor = UIColor(netHex: 0xFFC107)
                currentYear = UIColor(netHex: 0xFFC107)
            }
            return (salesColor, statsColor,currentYear,currentYearTextColor)
        }
    }
    
    @IBOutlet weak var sales: UILabel!
    @IBOutlet weak var budget: UILabel!
    @IBOutlet weak var year1: UILabel!
    @IBOutlet weak var year2: UILabel!
    @IBOutlet weak var year3: UILabel!
    @IBOutlet weak var percentage: UILabel!
    
    @IBOutlet var containers: [UIView]!
    let style = PublishSubject<SaleCollectionViewCell.Style>()
    let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       configView()
    }
    
    func configView()  {
        let scaleFactor: CGFloat = 0.7
        sales.minimumScaleFactor = scaleFactor
        sales.adjustsFontSizeToFitWidth = true
        budget.minimumScaleFactor = scaleFactor
        budget.adjustsFontSizeToFitWidth = true
        year1.minimumScaleFactor = scaleFactor
        year1.adjustsFontSizeToFitWidth = true
        year2.minimumScaleFactor = scaleFactor
        year2.adjustsFontSizeToFitWidth = true
        year3.minimumScaleFactor = scaleFactor
        year3.adjustsFontSizeToFitWidth = true
        percentage.minimumScaleFactor = scaleFactor
        percentage.adjustsFontSizeToFitWidth = true
        
    }
    
    func config(saleViewModel: SaleViewModel)  {
        saleViewModel.sales.asDriver().drive(sales.rx.text)
        .disposed(by: disposeBag)
        
        saleViewModel.budget.asDriver().drive(budget.rx.text)
            .disposed(by: disposeBag)
        
        saleViewModel.year1.asDriver().drive(year1.rx.text)
            .disposed(by: disposeBag)
        
        saleViewModel.year2.asDriver().drive(year2.rx.text)
            .disposed(by: disposeBag)
        
        saleViewModel.year3.asDriver().drive(year3.rx.text)
            .disposed(by: disposeBag)

        saleViewModel.percentage.asDriver().drive(percentage.rx.text)
            .disposed(by: disposeBag)
        
        style.subscribe(onNext: { (style) in
            let styling = style.styling()
            self.containers.first?.backgroundColor = styling.sales
            for i in 1 ..< self.containers.count{
                self.containers[i].backgroundColor = styling.stats
            }
            //text color for current year
            self.containers[2].backgroundColor = styling.currentYear
            self.year1.textColor = styling.currentYearTextColor
        }).disposed(by: disposeBag)
    }

}
