//
//  DatePickerView.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 02/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

protocol DatePickerViewDelegate{
    func applyDateFilter(from: Date, to: Date)
}
class DatePickerView: UIView, NibLoadableView, CustomView {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnFrom: UIButton!
    @IBOutlet weak var btnTo: UIButton!
    
    var contentView: UIView!
    
    var isSelectedFrom = true
    var toDate: Date!
    var fromDate: Date!
    var delegate: DatePickerViewDelegate?
    
    static func instance() -> DatePickerView{
        let frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        return DatePickerView(frame: frame)
    }
    
    override required init(frame: CGRect) {
        super.init(frame: frame)
        contentView = commonInit(bundle: .main)
        configView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        contentView = commonInit(bundle: .main)
        configView()
    }
    
    func configView()  {
        self.backgroundColor = UIColor.black.withAlphaComponent(0.3)
        let scaleFactor: CGFloat = 0.7
        btnTo.titleLabel?.minimumScaleFactor = scaleFactor
        btnTo.titleLabel?.adjustsFontSizeToFitWidth = true
        btnFrom.titleLabel?.minimumScaleFactor = scaleFactor
        btnFrom.titleLabel?.adjustsFontSizeToFitWidth = true
    }
    
    func config(from: Date, to: Date, isSelectedFrom: Bool)  {
        self.toDate = to
        self.fromDate = from
        datePicker.maximumDate = Date()
        btnFrom.setTitle(" From: \(fromDate.string(format: Filter.dateFormat)) ", for: .normal)
        btnTo.setTitle(" To: \(toDate.string(format: Filter.dateFormat)) ", for: .normal)
        
        self.isSelectedFrom = isSelectedFrom
        changeButton(btn: btnFrom, state: isSelectedFrom)
        changeButton(btn: btnTo, state: !isSelectedFrom)
    }
    
    func show()  {
        UIView.animate(withDuration: 0.3) {
            self.alpha = 1
        }
    }
    
    func hide(completion:@escaping ()-> Void)  {
        UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 0
        }) { (finished) in
            completion()
        }
    }
    
    @IBAction func dateTypeChanged(_ sender: UIButton) {
        if sender == btnTo{
            datePicker.setDate(toDate, animated: false)
            changeButton(btn: btnFrom, state: false)
            isSelectedFrom = false
        }else{
            datePicker.setDate(fromDate, animated: false)
            changeButton(btn: btnTo, state: false)
            isSelectedFrom = true
        }
        changeButton(btn: sender, state: true)
    }
    
    func changeButton(btn: UIButton, state: Bool)  {
        if state{
            btn.backgroundColor = UIColor.primary()
            btn.setTitleColor(UIColor.white, for: .normal)
        }else{
            btn.backgroundColor = UIColor.white
            btn.setTitleColor(UIColor.black, for: .normal)
        }
    }
    
    @IBAction func actDateChanged(_ sender: UIDatePicker) {
        let dateString = sender.date.string(format: Filter.dateFormat)
        if isSelectedFrom{
            btnFrom.setTitle(" From: \(dateString) ", for: .normal)
            fromDate = sender.date
        }else{
            btnTo.setTitle(" To: \(dateString) ", for: .normal)
            toDate = sender.date
        }
    }
    
    @IBAction func actDone(_sender: UIButton)  {
        delegate?.applyDateFilter(from: fromDate, to: toDate)
    }
}
