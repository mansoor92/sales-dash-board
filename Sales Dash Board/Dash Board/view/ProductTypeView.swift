//
//  FilterView.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 01/12/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit
import RxSwift

class ProductTypeView: UIView {

    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnGreen: UIButton!
    @IBOutlet weak var btnYellow: UIButton!
    @IBOutlet weak var btnRed: UIButton!
    @IBOutlet var selectionIndicator: UIView!
    @IBOutlet var selectionIndicatorLeading: NSLayoutConstraint!

	var selectedButton: UIButton?
	var selectedFilter = Variable<ProductType>(.all)

    let disposeBag = DisposeBag()


    override func awakeFromNib() {
        configView()
    }
    func configView()  {
		btnAll.layer.cornerRadius = 25
		btnGreen.layer.cornerRadius = 25
		btnYellow.layer.cornerRadius = 25
		btnRed.layer.cornerRadius = 25
		selectionIndicator.layer.cornerRadius = 1.5
    }

	@IBAction func actAll(_ sender: UIButton) {
		selectButton(sender: sender, productType: .all)
	}
	@IBAction func actGreen(_ sender: UIButton) {
		selectButton(sender: sender, productType: .green)
	}
	@IBAction func actYellow(_ sender: UIButton) {
		selectButton(sender: sender, productType: .yellow)
	}
	@IBAction func actRed(_ sender: UIButton) {
		selectButton(sender: sender, productType: .red)
	}

   private func selectButton(sender: UIButton, productType: ProductType)  {
        //filter is not already selected
        guard sender != selectedButton else {
            return
        }

        //select new filter
		selectedButton = sender
		selectedFilter.value = productType
		selectionIndicatorLeading.constant = sender.frame.origin.x
		UIView.animate(withDuration: 0.3) { [weak self] in
			self?.layoutIfNeeded()
		}
    }
}
