//
//  SaleViewModel.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation
import RxSwift

struct SaleViewModel {
    
    private var saleItem: SaleItem!
    
    var sales: Variable<String>
    var budget: Variable<String>
    var year1: Variable<String>
    var year2: Variable<String>
    var year3: Variable<String>
    var percentage: Variable<String>
    
    init(saleItem: SaleItem) {
        self.saleItem = saleItem
        sales = Variable(saleItem.getSales())
        budget = Variable(saleItem.getBudget())
        year1 = Variable(saleItem.getYear1())
        year2 = Variable(saleItem.getYear2())
        year3 = Variable(saleItem.getYear3())
        percentage = Variable(saleItem.getPercentage())
    }
}
