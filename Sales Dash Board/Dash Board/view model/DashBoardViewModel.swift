//
//  DashBoardViewModel.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum ApiCallStatus {
    case refreshData
    case showError(err: Error)
    case loading
}
class DashBoardViewModel {
    
    private var sales: [SaleItem]!
    let provider = MoyaProvider<DashBoardEndPoints>()
    let disposeBag = DisposeBag()
    
    var apiCallStatus = PublishSubject<ApiCallStatus>()
    var filter = Variable(Filter())
    
    init(sales: [SaleItem]) {
        self.sales = sales
        filter.asObservable()
            .skip(1)
            .subscribe(onNext: { (filter) in
                self.fetchSales(filter: filter)
            }).disposed(by: disposeBag)
    }
    
    func noOfItems() -> Int {
        return sales.count
    }
    
    func dataForItem(atIndexPath: IndexPath) -> SaleViewModel {
        return SaleViewModel(saleItem: sales[atIndexPath.row])
    }
    
    func fetchSales(filter: Filter)  {
        apiCallStatus.onNext(ApiCallStatus.loading)
        
        provider.rx.request(DashBoardEndPoints.sales(filter: filter))
            .debug("DashBoardEndPoints", trimOutput: false)
            .map({ (response) -> [SaleItem] in
                let salesItems = try? JSONDecoder().decode([SaleItem].self, from: response.data)
                guard salesItems != nil else{
                    return []
                }
                return salesItems!
            })
            .subscribe(onSuccess: { (saleItems) in
                self.sales = saleItems
                self.apiCallStatus.onNext(ApiCallStatus.refreshData)
                },
                onError: { (err) in
                    self.apiCallStatus.onNext(ApiCallStatus.showError(err: err))
            })
            .disposed(by: disposeBag)
    }
}
