//
//  Filter.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 29/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation

enum FilterType: Int, Codable{
    case custom = 0
    case today = 1
    case week = 2
    case month = 3
    case tillnow = 4
    case annual = 5
}
enum SaleType: Int,Codable{
    case sale = 1
    case collection = 2
}
enum ProductType: Int, Codable {
	case all = 0
	case green = 1
	case yellow = 2
	case red = 3
}
class Filter: Codable {
    static var dateFormat = "dd-MM-YYY"
    static var dateFormatForSending = "YYY-MM-dd"
    var saleType = SaleType.sale
    var type: FilterType = .month
	var productType = ProductType.all
    var fromDate: Date?
    var toDate: Date?
    
    func getFromDateString() -> String {
        guard let fromDate = fromDate?.string(format: Filter.dateFormatForSending) else{
            return "1990-01-01"
        }
        return fromDate
    }
    
    func toDateString() -> String {
        guard let fromDate = toDate?.string(format: Filter.dateFormatForSending) else{
            return "1990-01-01"
        }
        return fromDate
    }
    
}
