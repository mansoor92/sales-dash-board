//
//  Sales.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation

class SaleItem: Codable {
    var Actual: String?
    var Actual1: String?
    var Actual2: String?
    var Budget: String?
    var Sales: String?
    var Per: String?
    
    func getSales() -> String {
        return Sales ?? ""
    }
    func getBudget() -> String {
        return Budget ?? ""
    }
    func getYear1() -> String {
        return Actual ?? ""
    }
    func getYear2() -> String {
        return Actual1 ?? ""
    }
    func getYear3() -> String {
        return Actual2 ?? ""
    }
    func getPercentage() -> String {
        return Per ?? ""
    }
}
