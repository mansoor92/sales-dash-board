//
//  DashBoardCoordinator.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

class DashBoardCoordinator: Coordinator {
    
    weak var window: UIWindow!
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        guard let vc = StoryBoards().main().instantiateViewController(withIdentifier: DashBoardViewController.sceneName) as? DashBoardViewController else{
            fatalError("Unable to Load \(DashBoardViewController.sceneName)")
        }
        let sales = [SaleItem]()
        let viewModel = DashBoardViewModel(sales: sales)
        vc.viewModel = viewModel
        window.rootViewController = vc
    }
}
