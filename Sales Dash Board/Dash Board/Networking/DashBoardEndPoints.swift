//
//  DashBoardApi.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import Foundation
import Moya
import RxSwift


enum DashBoardEndPoints {
    case sales(filter: Filter)
}

extension DashBoardEndPoints: TargetType{
    
    public var baseURL: URL {
        return Api.baseUrl
    }
    
    public var headers: [String : String]? {
        return Api.headers
    }
    
    public var path: String {
        switch  self{
        case .sales(let filter):
            let deviceID = UIDevice.current.identifierForVendor?.uuidString ?? ""
			let params = "\(filter.saleType.rawValue)/\(filter.type.rawValue)/\(filter.getFromDateString())/\(filter.toDateString())/\(filter.productType.rawValue)/\(deviceID)"
//			http://221.120.219.5/APISalesDashboardApp/dashboard/getdata/1/1/1900-01-01/1900-01-01/0/863518032153071
            return "/dashboard/getdata/\(params)"
        }
    }
    
    public var method: Moya.Method {
        return .get
    }
    
    public var sampleData: Data {
        return "".data(using: .utf8)!
    }
    
    public var task: Task {
        return .requestPlain
    }
}

