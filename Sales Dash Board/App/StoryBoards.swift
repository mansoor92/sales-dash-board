//
//  StoryBoards.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

struct StoryBoards {
    func main() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: .main)
    }
}
