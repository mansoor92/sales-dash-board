//
//  AppCoordinator.swift
//  Sales Dash Board
//
//  Created by Mansoor Ali on 28/11/2017.
//  Copyright © 2017 Mansoor Ali. All rights reserved.
//

import UIKit

protocol Coordinator {
    func start()
}

class AppCoordinator: Coordinator{
    
    weak var window: UIWindow!
    
    let keyDashBoard = "DashBoard"
    var coordinators = [String: Coordinator]()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        let dashBoardCoordinator = DashBoardCoordinator(window: window)
        dashBoardCoordinator.start()
        coordinators[keyDashBoard] = dashBoardCoordinator
    }
}
